//
//  HistoryViewController.swift
//  TMCShare
//
//  Created by Chính Trình Quang on 1/22/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import UIKit
import  CoreData
class HistoryViewController: UIViewController {
    var list  = [HistoryDetail](){
        didSet{
            table.reloadData()
        }
    }
    @IBOutlet weak var table: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)
        setupView()
        setupRightButton()
        table.register(UINib(nibName: "FileListTableViewCell", bundle: nil), forCellReuseIdentifier: "FileListTableViewCell")
        table.delegate = self
        table.dataSource = self
        list.removeAll()
        for x in fetchData(entityname: "History"){
           
            list.append(x)
        }
    }
    func setupView (){
        self.navigationItem.title = "History"
        self.navigationController?.navigationBar.barStyle = UIBarStyle.blackTranslucent
    }
    func fetchData (entityname : String)-> [HistoryDetail]{
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let context = appDelegate.persistentContainer.viewContext
        
        //let entity = NSEntityDescription.entity(forEntityName: "EmployeeData", in: context)
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityname)
        //request.predicate = NSPredicate(format: "age = %@", "12")
        request.returnsObjectsAsFaults = false
        var cloneArray : [HistoryDetail] = [HistoryDetail]()
        do {
            let result = try context.fetch(request)
            if let results = result as? [NSManagedObject]{
                for data in results  {
                    
                    let detail = HistoryDetail()
                    if let t = data.value(forKey : "fileURL") as? String{
                        detail.url = t
                        
                    }
                    if let t = data.value(forKey : "time") as? String{
                        detail.time = t
                        
                    }
                    
                    
                    cloneArray.append(detail)
                }
            }
            
        } catch {
            
            
            print("Failed fetching data")
        }
        return cloneArray
    }
    func setupRightButton() {
        let rightbtn = UIButton(type: .system)
        rightbtn.setTitle("Delete history", for: .normal)
        rightbtn.addTarget(self, action: #selector(delAllHistory), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightbtn)
    }
    @objc func delAllHistory (){
        deleteData(entityname: "History")
        list.removeAll()
    }
    func deleteData(entityname : String) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: entityname)
        
        
        
        do {
            let results = try context.fetch(fetchRequest)
            if results.count > 0{
                for x in results {
                    let objectDelete = x as NSManagedObject
                    context.delete(objectDelete)
                    do{
                        try context.save()
                    }catch{
                        print(error)
                    }
                }
                
                
            }
            
        }
        catch {
            print("error executing fetch request: \(error)")
        }
        
        
    }

}
extension HistoryViewController : UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        FullScreenVC.url = list[indexPath.row].url!
        self.present(UINavigationController(rootViewController: FullScreenVC()), animated: true, completion: nil)
        
    }
}
extension HistoryViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = table.dequeueReusableCell(withIdentifier:"FileListTableViewCell", for: indexPath) as? FileListTableViewCell  else {
            return UITableViewCell()
            
        }
        cell.setCell(name: list[indexPath.row].time!, url: list[indexPath.row].url!)
        return cell
    }
    
    
}
