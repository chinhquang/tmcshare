//
//  ScanViewController.swift
//  TMCShare
//
//  Created by Chính Trình Quang on 1/22/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import UIKit
import AVFoundation
class ScanViewController: UIViewController,AVCaptureMetadataOutputObjectsDelegate {

    var isBack = true
    var device = AVCaptureDevice.default(for: .video)
    @objc func switchbtnClick(){
        captureSession.stopRunning()
        if isBack == true{
            isBack = false
            if let device = AVCaptureDevice.DiscoverySession(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera], mediaType: AVMediaType.video, position: AVCaptureDevice.Position.front).devices.first{
                self.device = device
            }
        }else {
            isBack = true
            if let device = AVCaptureDevice.DiscoverySession(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera], mediaType: AVMediaType.video, position: AVCaptureDevice.Position.back).devices.first{
                self.device = device
            }
        }
        startSession()
        
    }
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    func setupView (){
        self.navigationItem.title = "Scan"
        self.navigationController?.navigationBar.barStyle = UIBarStyle.blackTranslucent
    }
    func setRightBar() -> Void {
        let switchbtn = UIButton(type: .system)
        switchbtn.setImage(#imageLiteral(resourceName: "icons8-rotate-camera-48").withRenderingMode(.automatic), for: .normal)
        switchbtn.addTarget(self, action: #selector(switchbtnClick), for: .touchUpInside)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: switchbtn)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    func startSession() -> Void {
        
        captureSession = AVCaptureSession()
        
        guard let videoCaptureDevice = self.device else { return }
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }
        
        let metadataOutput = AVCaptureMetadataOutput()
        
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]
        } else {
            failed()
            return
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        view.layer.addSublayer(previewLayer)
        
        captureSession.startRunning()
    }
    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.backgroundColor = UIColor.black
        setupView()
        setRightBar()
        startSession()
        if (captureSession?.isRunning == false) {
            captureSession.startRunning()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
        
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession.stopRunning()
        
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: stringValue)
            if let data = stringValue.data(using: .utf8){
                do{
                    ListFileToDownloadVC.URLList.removeAll()
                    if let array = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)  as? [AnyObject]{
                        for x in array
                        {
                            let t = x as! [String:Any]
                            if let url = t["url"] as? String{
                                ListFileToDownloadVC.URLList.append(url)
                            }
                        }
                        if array.count == 0{
                            let alert = UIAlertController(title: "QRScan warning", message: "There is no infomation in this QR image", preferredStyle: .actionSheet)
                            alert.view.tintColor = UIColor.black
                            
                            
                            alert.addAction(UIAlertAction(title: "Cancel", style: .default) { [unowned self]  result in
                                self.captureSession.startRunning()
                                
                                
                            })
                            present(alert, animated: true, completion: nil)
                        }else{
                            let alert = UIAlertController(title: "QRScan", message: "Scan successfully. Show all information ?", preferredStyle: .actionSheet)
                            alert.view.tintColor = UIColor.black
                            
                            
                            alert.addAction(UIAlertAction(title: "OK", style: .default) { [unowned self]  result in
                                
                                self.navigationController?.pushViewController(ListFileToDownloadVC(), animated: true)
                                
                            })
                            
                            alert.addAction(UIAlertAction(title: "Cancel", style: .default) { [unowned self]  result in
                                self.captureSession.startRunning()
                                
                                
                            })
                            present(alert, animated: true, completion: nil)
                            

                        }
                        
                    }
                }catch{
                    
                    print(error)
                    
                    
                    let alert = UIAlertController(title: "QRScan Error", message: "Not in right format", preferredStyle: .alert)
                    alert.view.tintColor = UIColor.black
                    
                    alert.addAction(UIAlertAction(title: "OK", style: .default) { [unowned self]  result in
                        self.captureSession.startRunning()
                        
                        
                    })
                    
                    present(alert, animated: true, completion: nil)
                    
                
                
                }
            }
            
            
            
        }
        
        //
    }
    
    func found(code: String) {
        print(code)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
}
