//
//  ShareViewController.swift
//  TMCShare
//
//  Created by Chính Trình Quang on 1/22/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import UIKit
import Foundation
import QuickLook
import Alamofire
import MobileCoreServices
import CoreData

class ShareViewController: UIViewController{
    var isTurnOnChoosing = 0
    static var currentDirPath = ""
    static var dirList = [""]
    var activityIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
    let quickLookController = QLPreviewController()
    @IBOutlet weak var folderList: UITableView!
    @IBOutlet var btnMenu: [UIButton]!
    @IBOutlet var btnOptions: [UIButton]!
    @IBOutlet weak var heightConstant: NSLayoutConstraint!
    var list : [CellType] = [CellType](){
        didSet{
            folderList.reloadData()
        }
    }
    var fileURLList = [NSURL]()
    var isChoosingList = [Bool]()
    let imgPicker = UIImagePickerController()

    @IBAction func getImage(_ sender: Any) {
        tapOutSide()
        imgPicker.allowsEditing = false
        imgPicker.sourceType = .photoLibrary
        present(imgPicker, animated: true, completion: nil)
    }
    @IBAction func shareFile(_ sender: Any) {
        showOptionSelectingCell()
        isTurnOnChoosing = 1
        tapOutSide()
        
    }
    
    @IBAction func selectAllItem(_ sender: Any) {
       
        for i in 0...list.count - 1{
            guard let cell = folderList.cellForRow(at: IndexPath(row: i, section: 0))else {
                return
            }
            cell.accessoryType = .checkmark
            isChoosingList[i] = true
        }
    }
    @IBAction func cancelSelection(_ sender: Any) {
        hideOptionSelectingCell()
        for i in 0..<list.count {
            guard let cell = folderList.cellForRow(at: IndexPath(row: i, section: 0))else {
                return
            }
            cell.accessoryType = .none
            isChoosingList[i] = false
        }
        
    }
    
    @IBAction func makedir(_ sender: Any) {
        tapOutSide()
        var newFolderName : String?
        let alert = UIAlertController(title: "New folder", message: "Type new folder name", preferredStyle: .alert)
        alert.view.tintColor = UIColor.blue
        //2. Add the text field. You can configure it however you need.
        alert.addTextField(configurationHandler: { (textField) -> Void in
            textField.text = "New folder"
            UI.addDoneButton(controls: [textField])
        })
        
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (action) -> Void in
            
            newFolderName = alert!.textFields![0].text
            for x in self.list{
                if x.name == newFolderName{
                    return
                }
            }
            
            self.createNewFolder(withname: newFolderName!)
            self.listContents()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) -> Void in
            
        }))
        present(alert, animated: true,completion: nil)
        
        
    }
    func createNewFolder( withname name : String) {
        //create directory
        let newdir = getFullCurrentDirectoryPath() + "/\(name)"
        
        do{
            try FileManager.default.createDirectory(atPath:  newdir, withIntermediateDirectories: true, attributes: nil)
            
        }catch let error as NSError{
            print("Unable to create directory",error)
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)
        ShareViewController.currentDirPath = ShareViewController.dirList.last!
        print(getFullCurrentDirectoryPath())
        setupView()
    
        folderList.register(UINib(nibName: "FileListTableViewCell", bundle: nil), forCellReuseIdentifier: "FileListTableViewCell")
        
        folderList.delegate = self
        folderList.dataSource = self
        folderList.tableFooterView = UIView()
        
        quickLookController.dataSource = self
        listContents()
        if ShareViewController.dirList.count >= 2{
            setBackButton()
        }else {
             navigationItem.hidesBackButton = true;
        }
        imgPicker.delegate = self
       tapOutSide()
       hideOptionSelectingCell()
    }
    @objc func showMenu() -> Void{
        btnMenu.forEach{(button) in
            button.isHidden = !button.isHidden
            if button.isHidden == true{
                heightConstant.constant = 0
            }else {
                heightConstant.constant = 100
            }
        }
    }
    
    
    @objc func tapOutSide() -> Void{
        btnMenu.forEach{(button) in
            if button.isHidden != true {
                button.isHidden = true
                heightConstant.constant = 0
            }
            
        }
    }
    @objc func hideOptionSelectingCell() -> Void{
         btnOptions.forEach{(button) in
            if button.isHidden != true {
                button.isHidden = true
               
            }
            
        }
        isTurnOnChoosing = 0
    }
    @objc func showOptionSelectingCell() -> Void{
        btnOptions.forEach{(button) in
            button.isHidden = !button.isHidden
           
        }
    }
    func setBackButton() -> Void {
        let backButton = UIButton(type: .system)
        backButton.setTitle("Back", for: .normal)
        backButton.addTarget(self, action: #selector(gotoBack), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
        
    }
    func setRightButton (){
        let rightButton = UIButton(type: .system)
        rightButton.setImage(#imageLiteral(resourceName: "menu").withRenderingMode(.automatic), for: .normal)
        rightButton.addTarget(self, action: #selector(showMenu), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightButton)
    }
    
    @objc func gotoBack() {
        let svc = ShareViewController()
        ShareViewController.dirList.removeLast()
        
        navigationController?.pushViewController(svc, animated: true)
        
    }
    func setupView (){
        self.navigationItem.title = "Share"
        self.navigationController?.navigationBar.barStyle = UIBarStyle.blackTranslucent
        setRightButton()
    }
   
    @IBAction func shareSelection(_ sender: Any) {
        startIndicator()
        var count = 0
        var selectionCount = 0
        for i in 0..<isChoosingList.count{
            
            if isChoosingList[i] == true{
                selectionCount = selectionCount + 1
                if fileIsDir(path: fileURLList[i].path!){
                    count = count + 1
                }
            }
        }
        if selectionCount == 0{
            let alert = UIAlertController(title: "ERROR", message: "You must choose at least one file to share", preferredStyle: .alert)
            alert.view.tintColor = UIColor.black
            alert.addAction(UIAlertAction(title: "OK", style: .default) {  result in
                
                return
            })
            present(alert, animated: true, completion: nil)
            return
        }
        else if count != 0{
            let alert = UIAlertController(title: "ERROR", message: "Just share only files", preferredStyle: .alert)
            alert.view.tintColor = UIColor.black
            alert.addAction(UIAlertAction(title: "OK", style: .default) {  result in
                return
                
            })
            present(alert, animated: true, completion: nil)
            
        }else {
            var urllist = [NSURL]()
            for i in 0..<fileURLList.count{
                if isChoosingList[i] == true{
                    urllist.append(fileURLList[i])
                    
                }
            }
            
            API.uploadMultipleFile(fileURL: urllist){
                response in
                let json = QRObject(JSON: response)
                if let url = json?.qr_image{
                    self.saveRecord(entityname: "History", value: url, forkey: "fileURL")
                    print(url)
                    FullScreenVC.url = url
                    self.stopIndicator()
                    
                    self.present(UINavigationController(rootViewController: FullScreenVC()), animated: true, completion: nil)
                    
                }
                
                
            }
        }
        
    }
    
    func listContents() {
        //create directory
        let fileManager = FileManager.default
        do {
            let arr = try fileManager.contentsOfDirectory(atPath: getFullCurrentDirectoryPath())
            list.removeAll()
            fileURLList.removeAll()
            isChoosingList.removeAll()
            for x in arr{
                let newItem = CellType()
                let itemURL = getFullCurrentDirectoryPath() + "/\(x)"
                
                
                if fileIsDir(path: itemURL){
                    newItem.type = FileType.folder
                }else {
                    let t = x.extractFilename()
                    if t.fileExtension == "png" || t.fileExtension == "jpeg" || t.fileExtension == "jpg" || t.fileExtension == "PNG" || t.fileExtension == "JPEG" || t.fileExtension == "JPG" {
                        newItem.type = FileType.imageFile
                    }else {
                        newItem.type = FileType.other
                    }
                    
                }
                newItem.name = x
                list.append(newItem)
                
                let url = getFullCurrentDirectoryPath() + "/\(x)"
                fileURLList.append(NSURL(fileURLWithPath: url))
                isChoosingList.append(false)
            }
            print(try! fileManager.contentsOfDirectory(atPath: getFullCurrentDirectoryPath()))
            
        }catch{
            print(error.localizedDescription)
        }
        
        
    }
    
    func getFullCurrentDirectoryPath ()-> String{
        
        return  baseURL + "/" + ShareViewController.currentDirPath
        
    }
    var baseURL : String = {
        let path = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory
            , .userDomainMask, true)[0] as String
        
        
        
        return path
    }()
    
    func fileIsDir(path: String) -> Bool {
        var isDir: ObjCBool = false;
        FileManager.default.fileExists(atPath: path, isDirectory: &isDir)
        return isDir.boolValue
    }
    func remove(filename : String) -> Void {
        let urlString = getFullCurrentDirectoryPath() + "/\(filename)"
        let url = URL(fileURLWithPath: urlString)
        do{
            try FileManager.default.removeItem(at: url)
        }catch{
            print("cant remove file...")
        }
    }
    func saveRecord (entityname : String, value : String, forkey key : String){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let context = appDelegate.persistentContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: entityname, in: context)
        let newUser = NSManagedObject(entity: entity!, insertInto: context)
        
        
        newUser.setValue(value, forKey: key)
        newUser.setValue(currentTime, forKey: "time")
//
        
        do {
            
            try context.save()
            
        } catch {
            
            print("Failed saving")
        }
    }
    
    
    var currentTime : String = {
        let date = Date()
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day ,.hour,.minute,.second], from: date)
        
       
        let year = String(components.year!)
        let month = String(components.month!)
        let day = String(components.day!)
        let hour = String(components.hour!)
        let minute = String(components.minute!)
        let second = String(components.second!)
        return year + "-" + month + "-" + day + "|"
        + hour + ":" + minute + ":" + second
    }()
    func startIndicator()
    {
        //creating view to background while displaying indicator
        let container: UIView = UIView()
        container.frame = self.view.frame
        container.center = self.view.center
        container.backgroundColor = UIColor.lightGray
        
        //creating view to display lable and indicator
        let loadingView: UIView = UIView()
        loadingView.frame = CGRect(x: 0, y: 0, width: 118, height: 80)
        loadingView.center = self.view.center
        loadingView.backgroundColor = UIColor.darkGray
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        //Preparing activity indicator to load
        self.activityIndicator = UIActivityIndicatorView()
        self.activityIndicator.frame = CGRect(x: 40, y: 12, width: 40, height: 40)
        self.activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
        loadingView.addSubview(activityIndicator)
        
        //creating label to display message
        let label = UILabel(frame: CGRect(x: 5, y : 55, width :120,height :20))
        label.text = "Uploading..."
        label.textColor = UIColor.white
        label.font = UIFont(name: label.font.fontName, size: 12)
        label.bounds = CGRect(x : 0, y: 0, width : loadingView.frame.size.width / 2, height : loadingView.frame.size.height / 2)
        loadingView.addSubview(label)
        container.addSubview(loadingView)
        self.view.addSubview(container)
        
        self.activityIndicator.startAnimating()
    }
    func stopIndicator()
    {
        UIApplication.shared.endIgnoringInteractionEvents()
        self.activityIndicator.stopAnimating()
        ((self.activityIndicator.superview as UIView!).superview as UIView!).removeFromSuperview()
    }

}
extension ShareViewController:UITableViewDelegate,UITableViewDataSource, QLPreviewControllerDelegate, QLPreviewControllerDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    
    func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return list.count
    }
    
    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        return fileURLList[index]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = folderList.dequeueReusableCell(withIdentifier: "FileListTableViewCell", for: indexPath) as? FileListTableViewCell else { return UITableViewCell()}
        cell.setCell(name: list[indexPath.row].name!, thumbnail: fileURLList[indexPath.row].path!)
        cell.tintColor = UIColor.red
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isTurnOnChoosing == 0{
            if list[indexPath.row].type == FileType.folder{
                let svc = ShareViewController()
                ShareViewController.dirList.append(list[indexPath.row].name!)
                ShareViewController.currentDirPath = ShareViewController.currentDirPath + "/" + list[indexPath.row].name!
                navigationController?.pushViewController(svc, animated: true)
            }else {
                if QLPreviewController.canPreview(fileURLList[indexPath.row]) {
                    quickLookController.currentPreviewItemIndex = indexPath.row
                    navigationController?.pushViewController(quickLookController, animated: true)
                }
            }
        }else {
            guard let cell  = folderList.cellForRow(at: indexPath) as? FileListTableViewCell else {
                return
            }
            if isChoosingList[indexPath.row] == true{
                isChoosingList[indexPath.row] = false
                cell.accessoryType = .none
                
            }else{
                isChoosingList[indexPath.row] = true
                cell.accessoryType = .checkmark
            }
            print(isChoosingList)
        }
        
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            print("delete")
            remove(filename: list[indexPath.row].name!)

            listContents()



        }

    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let fileUrl = info[UIImagePickerController.InfoKey.imageURL] as? URL else { return }
        
        print(fileUrl.path)
        let filename  = fileUrl.lastPathComponent
        
        let copyItemDir = getFullCurrentDirectoryPath() + "/\(filename)"
        print(copyItemDir)
        
        do{
            try FileManager.default.copyItem(at: fileUrl, to: URL(fileURLWithPath: copyItemDir))

        }catch{
            print(error.localizedDescription)
        }
        dismiss(animated: true, completion: nil)
        
    }
}
