//
//  TabBarViewController.swift
//  TMCShare
//
//  Created by Chính Trình Quang on 1/22/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.barTintColor = UIColor.black
        //tab scan
        let scanVC = ScanViewController()
        scanVC.tabBarItem = UITabBarItem(title: "", image: #imageLiteral(resourceName: "scan"), tag: 0)
        let nav1 = UINavigationController (rootViewController: scanVC)
       
       
        
        
        //tab history
        let historyVC = HistoryViewController()
        historyVC.tabBarItem = UITabBarItem(title: "", image: #imageLiteral(resourceName: "history"), tag: 1)
        let nav2 = UINavigationController (rootViewController: historyVC)
        
        //tab history
        let shareVC = ShareViewController()
        shareVC.tabBarItem = UITabBarItem(title: "", image: #imageLiteral(resourceName: "share"), tag: 1)
        let nav3 = UINavigationController (rootViewController: shareVC)
        
        // set up tabbar
        let tabBarList = [nav1, nav2, nav3]
        viewControllers = tabBarList

        // Do any additional setup after loading the view.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
