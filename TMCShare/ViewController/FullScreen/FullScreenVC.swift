//
//  FullScreenVC.swift
//  TMCShare
//
//  Created by Chính Trình Quang on 1/28/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import UIKit

class FullScreenVC: UIViewController,UIGestureRecognizerDelegate {

    
    static var url  = String()
    @IBAction func close(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBOutlet var recognizer: UITapGestureRecognizer!
    @IBOutlet weak var close_button: UIButton!
    @IBOutlet weak var ImageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        close_button.isHidden = true
        
        
    }
    func setRightButton() -> Void {
        let rightBtn = UIButton(type: .system)
        self.navigationController?.isNavigationBarHidden = false
        rightBtn.addTarget(self, action: #selector(addActivityController), for: .touchUpInside)
        rightBtn.setImage(#imageLiteral(resourceName: "upload-symbol").withRenderingMode(.automatic), for: .normal)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightBtn)
        
    }
    @objc func addActivityController() -> Void {
        let itemshare : UIImage = ImageView.image!
        let activityVC = UIActivityViewController(activityItems: [itemshare], applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = self.view
        activityVC.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
        activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop,UIActivity.ActivityType.postToFacebook,UIActivity.ActivityType.assignToContact,UIActivity.ActivityType.mail,UIActivity.ActivityType.message ]
        self.present(activityVC, animated: true, completion: nil)

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setRightButton()
        self.recognizer.cancelsTouchesInView = false
        
        self.recognizer.delegate = self
        
        self.tabBarController?.tabBar.isHidden = true
        ImageView.setImg(url: FullScreenVC.url)
        
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        // Get the location in CGPoint
        let location = touch.location(in: nil)
        
        // Check if location is inside the view to avoid
        if close_button.frame.contains(location) {
            return false
        }
        
        return true
    }

    var isHiden : Bool = true {
        didSet{
            UIView.transition(with: close_button, duration: 0.5, options: .transitionCrossDissolve, animations: {
                if self.isHiden == true{
                    self.close_button.isHidden = true
                }else{
                    self.close_button.isHidden = false
                }
                
            })
        }
    }

    @IBAction func tapScreen(_ sender: Any) {
        isHiden = !isHiden
        print(1)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
