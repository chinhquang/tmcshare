//
//  ListFileToDownloadVC.swift
//  TMCShare
//
//  Created by Chính Trình Quang on 1/28/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import UIKit
import SafariServices
class ListFileToDownloadVC: UIViewController {
    var list : [CellType] = [CellType](){
        didSet{
            table.reloadData()
        }
    }
    static var URLList : [String] = [String]()
    @IBOutlet weak var table: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    func setupView (){
        self.navigationItem.title = "Choose to show content"
        self.navigationController?.navigationBar.barStyle = UIBarStyle.blackTranslucent
        
    }
    fileprivate func setList (){
        list.removeAll()
        for x in ListFileToDownloadVC.URLList{
            let i = NSURL(fileURLWithPath: x)
            let newName = i.lastPathComponent
            let newFile = CellType()
            newFile.name = newName
            let t = newName!.extractFilename()
            if t.fileExtension == "PNG" || t.fileExtension == "jpeg" || t.fileExtension == "jpg" {
                newFile.type = FileType.imageFile
            }else {
                newFile.type = FileType.other
            }
            list.append(newFile)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(animated)
        setupView()
        table.register(UINib(nibName: "FileListTableViewCell", bundle: nil), forCellReuseIdentifier: "FileListTableViewCell")
        table.delegate = self
        table.dataSource = self
        setList()
    }
    
    
}
extension ListFileToDownloadVC : SFSafariViewControllerDelegate{
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }

}
extension ListFileToDownloadVC : UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let url = URL(string: ListFileToDownloadVC.URLList[indexPath.row])
        let vc = SFSafariViewController(url: url!)
        present(vc, animated: true, completion: nil)
        vc.delegate = self

    }
}
extension ListFileToDownloadVC : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = table.dequeueReusableCell(withIdentifier: "FileListTableViewCell", for: indexPath) as? FileListTableViewCell else { return UITableViewCell()}
        cell.setCell(name: list[indexPath.row].name!, url: ListFileToDownloadVC.URLList[indexPath.row])
        cell.tintColor = UIColor.red
        return cell
    }
    
    
}
