//
//  UIImageView-Extension.swift
//  TMCShare
//
//  Created by Chính Trình Quang on 1/23/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation
import UIKit
extension UIImageView{
    func setImg(name : String){
        self.image = UIImage(named: name)
    }
    func setImg (url : String){
        guard let Url = URL(string: url)else {
            return
        }
        guard let data = try? Data(contentsOf: Url) else {
            return
        }
        self.image = UIImage(data: data)
    }
    func setImg(dirPath : String) {
        self.image = UIImage(contentsOfFile: dirPath)
    }
}
