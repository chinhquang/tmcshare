//
//  API.swift
//  TMCShare
//
//  Created by Chính Trình Quang on 1/27/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import MobileCoreServices
class API {
   
    static func mimeTypeForPath(path: String) -> String {
        let url = NSURL(fileURLWithPath: path)
        let pathExtension = url.pathExtension
        
        if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension! as NSString, nil)?.takeRetainedValue() {
            if let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() {
                return mimetype as String
            }
        }
        return "application/octet-stream"
    }
    static var serverURL = "https://tmlshare_service.ericadonis.xyz/v1/uploads"
    static func uploadFile(fileURL : NSURL, completion:  @escaping ([String:Any]) -> Void){
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            
            
            multipartFormData.append(URL(fileURLWithPath: fileURL.path!), withName: "file", fileName: fileURL.lastPathComponent!, mimeType: API.mimeTypeForPath(path: fileURL.path!))
        }, to: API.serverURL, method: .post, headers: nil,
           encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.response{
                    response in
                    print(response)
                }
                upload.validate(statusCode: 200..<300)
                    .responseJSON { response in
                        
                        if let json = response.result.value as? [String: Any]{
                            completion(json)
                        }

                }
            case .failure(let encodingError):
                print("error:\(encodingError)")
            }
        })
    }
    static func uploadMultipleFile(fileURL : [NSURL], completion:  @escaping ([String:Any]) -> Void){
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            for url in fileURL{
                
                multipartFormData.append(URL(fileURLWithPath: url.path!), withName: "file", fileName: url.lastPathComponent!, mimeType: API.mimeTypeForPath(path: url.path!))
            }
            
            
        }, to: API.serverURL, method: .post, headers: nil,
           encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                
                upload.validate(statusCode: 200..<300).responseJSON { response in
                        if let json = response.result.value as? [String: Any]{
                            completion(json)
                        }
                        
                }
            case .failure(let encodingError):
                print("error:\(encodingError)")
            }
        })
    }
    
}
