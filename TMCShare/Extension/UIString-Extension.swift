//
//  UIString-Extension.swift
//  TMCShare
//
//  Created by Chính Trình Quang on 1/23/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation

extension String{
    func listfile() -> [String.SubSequence] {
        return self.split(separator: "/")
    }
    func toURL() -> NSURL {
        return NSURL(fileURLWithPath: self)
    }
    func extractFilename() -> (fileName: String, fileExtension: String) {
        
        let filenameParts = self.components(separatedBy: ".")
        
        // Return file name and file extension
        return (filenameParts[0], filenameParts[1])
    }
}
