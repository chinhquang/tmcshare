//
//  FileType.swift
//  TMCShare
//
//  Created by Chính Trình Quang on 1/23/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation
enum FileType{
    case folder, imageFolder, imageFile, other
}
class CellType {
    var name : String?
    var type : FileType?
    init(){
        
    }
    init(name : String, type : FileType) {
        self.name = name
        self.type = type
    }
    
}
