//
//  QRObject.swift
//  TMCShare
//
//  Created by Chính Trình Quang on 1/28/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation
import ObjectMapper
class QRObject : Mappable{
    var qr_image : String?
    
    required init?(map: Map){
        
    }
    init() {
        
    }
    func mapping(map: Map) {
        qr_image <- map["qr_image"]
       
        
    }
}


