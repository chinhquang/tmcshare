//
//  PreviewView.swift
//  TMCShare
//
//  Created by Chính Trình Quang on 1/22/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import UIKit
import AVFoundation
class PreviewView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    var videoPreviewLayer : AVCaptureVideoPreviewLayer{
        if let layer = layer as? AVCaptureVideoPreviewLayer{
            return layer
        }
        return AVCaptureVideoPreviewLayer()
    }
    
    var session  :  AVCaptureSession?{
        get{
            return videoPreviewLayer.session
        }
        set{
            videoPreviewLayer.session = newValue
        }
    }
    override class var layerClass: AnyClass{
        return AVCaptureVideoPreviewLayer.self
    }
}
