//
//  FileListTableViewCell.swift
//  TMCShare
//
//  Created by Chính Trình Quang on 1/23/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import UIKit

class FileListTableViewCell: UITableViewCell {


    @IBOutlet weak var filename: UILabel!
    @IBOutlet weak var fileIcon: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setCell(name : String, type : FileType) {
        switch type {
        case .folder:
            self.fileIcon.image = #imageLiteral(resourceName: "icons8-folder-40")
        
        case .imageFolder:
            self.fileIcon.image = #imageLiteral(resourceName: "icons8-pictures-folder-40")
        case .imageFile:
            self.fileIcon.image = #imageLiteral(resourceName: "icons8-picture-40")
        case .other:
            self.fileIcon.image = #imageLiteral(resourceName: "icons8-document-40")
        }
        self.filename.text = name
        
    }
    func setCell(celltype : CellType) {
        guard let type = celltype.type else {return }
        switch type {
        case .folder:
            self.fileIcon.image = #imageLiteral(resourceName: "icons8-folder-40")
            
        case .imageFolder:
            self.fileIcon.image = #imageLiteral(resourceName: "icons8-pictures-folder-40")
        case .imageFile:
            self.fileIcon.image = #imageLiteral(resourceName: "icons8-picture-40")
        case .other:
            self.fileIcon.image = #imageLiteral(resourceName: "icons8-document-40")
        }
        self.filename.text = celltype.name
        self.accessoryType = .none
    }
    func setCell(name : String , thumbnail : String) {
        self.fileIcon.setImg(dirPath: thumbnail)
        self.filename.text = name
        self.accessoryType = .none
        
    }
    func setCell(name : String , url : String) {
        self.fileIcon.setImg(url: url)
        self.filename.text = name
        self.accessoryType = .none
        
    }
}
